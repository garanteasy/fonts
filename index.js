const svgtofont = require("svgtofont");
const path = require("path");

svgtofont({
    src: path.resolve(process.cwd(), "svg"), // svg path
    emptyDist: true,
    dist: path.resolve(process.cwd(), "font"), // output path
    outSVGReact: true,
    styleTemplates: path.resolve(process.cwd(), "styles"), // file templates path (optional)
    fontName: "garanteasy", // font name
    css: {
        fontSize: '10em'
    }, // Create CSS files.
    startUnicode: 0xea01, // unicode start number
    svgicons2svgfont: {
        //fontHeight: 1000,
        normalize: true
    },
    // website = null, no demo html files
    website: {
        title: "Font Garanteasy",
        // Must be a .svg format image.
        logo: path.resolve(process.cwd(), "svg", "git.svg"),
        version: '1.0',
        meta: {
            description: "Font Garanteasy convertiti da SVG nei formati TTF/EOT/WOFF/WOFF2/SVG.",
            keywords: "svgtofont,TTF,EOT,WOFF,WOFF2,SVG"
        },
        description: ``,
        // Add a Github corner to your website
        // Like: https://github.com/uiwjs/react-github-corners
        corners: {
            url: 'https://gitlab.com/garanteasy/fonts',
            width: 62, // default: 60
            height: 62, // default: 60
            bgColor: '#dc3545' // default: '#151513'
        },
        links: [
            {
                title: "GitLab",
                url: "https://gitlab.com/garanteasy/fonts"
            },
            {
                title: "Feedback",
                url: "https://gitlab.com/garanteasy/fonts/issues"
            },
            {
                title: "Font Class",
                url: "index.html"
            },
            {
                title: "Unicode",
                url: "unicode.html"
            }
        ],
        footerInfo: `Made for <a href="https://webapp.garanteasy.com">Garanteasy</a>`
    }
}).then(() => {
    console.log('done!');
});;
